import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  words = [];
  guessed: string;
  guess: string;
  hidden_word: string;
  word_to_be_guessed: string;

  constructor(private toastController: ToastController) {}

  ngOnInit() {
    this.words.push('mobile');
    this.words.push('programming');
    this.words.push('object-orientation');


    const random_number = this.random();
    this.word_to_be_guessed = this.words[random_number];
    const star: string = '*';
    this.hidden_word = star.repeat(this.word_to_be_guessed.length);
  }

  random(): number {
    const rand = Math.floor(Math.random() * this.words.length);
    return rand;
  }

  async check(event) {
    if (event.keyCode === 13 && this.guess.length > 0) {
      if (this.guess.length === 1)  {
          if (this.guessed != undefined)  {
            this.guessed = this.guessed + ',' + this.guess;
          } else {
            this.guessed = this.guess;
          }

          for (let i = 0;i < this.word_to_be_guessed.length;i++)  {
            if (this.word_to_be_guessed.substr(i,1) === this.guess) {
              this.hidden_word = this.hidden_word.substring(0, i) + this.guess + this.hidden_word.substring(i + 1);
            }
          }

      } else {
        if (this.guess === this.word_to_be_guessed){
          this.hidden_word = this.word_to_be_guessed;
        }
      }

      if (this.hidden_word === this.word_to_be_guessed) {
        const toast = await this.toastController.create({
          message: 'You have guessed the word!',
          position: 'top',
          duration: 3000
        });
        toast.present();
      }

      this.guess = '';
    }
  }
  }
